import request from '@/utils/request'

// 查询登录日志列表
export function list(query) {
  return request({
    url: '/system/loginLog/list',
    method: 'get',
    params: query
  })
}

// 删除登录日志
export function delloginLog(infoId) {
  return request({
    url: '/system/loginLog/' + infoId,
    method: 'delete'
  })
}

// 清空登录日志
export function cleanloginLog() {
  return request({
    url: '/system/loginLog/clean',
    method: 'delete'
  })
}

// 清空登录日志
export function cleanLogininfor() {
  return request({
    url: '/system/logininfor/clean',
    method: 'delete'
  })
}
