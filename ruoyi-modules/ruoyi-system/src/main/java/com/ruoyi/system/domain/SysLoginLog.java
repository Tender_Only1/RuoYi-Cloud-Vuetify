package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.annotation.Excel.ColumnType;
import com.ruoyi.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统访问记录表 sys_loginLog
 * 
 * @author ruoyi
 */
@ApiModel(value = "系统访问记录")
@Data
@EqualsAndHashCode(callSuper = true)
public class SysLoginLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @Excel(name = "序号", cellType = ColumnType.NUMERIC)
    private Long infoId;

    @ApiModelProperty(value = "用户账号")
    @Excel(name = "用户账号")
    private String userName;

    @ApiModelProperty(value = "状态 1成功 0失败")
    @Excel(name = "状态", readConverterExp = "1=成功,1=0=失败")
    private String status;

    @ApiModelProperty(value = "地址")
    @Excel(name = "地址")
    private String ipaddr;

    @ApiModelProperty(value = "描述")
    @Excel(name = "描述")
    private String msg;

    @ApiModelProperty(value = "访问时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "访问时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date accessTime;

}