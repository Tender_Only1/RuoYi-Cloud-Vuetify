package com.ruoyi.system.controller.elasticsearch;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.service.IElasticSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



/**
 * elastic search 接口测试控制类
 *
 * @author piapia
 */
@RestController
@RequestMapping("/elasticSearch")
public class ElasticSearchController extends BaseController {
    
    @Autowired
    private IElasticSearchService elasticSearchService;
    
    /**
     * 获取参数配置列表
     */
    @GetMapping("/testMatchQueryAll")
    public AjaxResult matchQueryAll() {
        elasticSearchService.matchQueryAll();
        return null;
    }
}
