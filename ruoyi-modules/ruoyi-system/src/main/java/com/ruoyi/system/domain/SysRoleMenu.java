package com.ruoyi.system.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 角色和菜单关联 sys_role_menu
 * 
 * @author ruoyi
 */
@ApiModel(value = "角色和菜单关联")
@Data
public class SysRoleMenu
{

    @ApiModelProperty(value = "角色ID")
    private Long roleId;
    
    @ApiModelProperty(value = "菜单ID")
    private Long menuId;

}
