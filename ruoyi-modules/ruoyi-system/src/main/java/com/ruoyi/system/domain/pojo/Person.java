package com.ruoyi.system.domain.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Person {
    
    @JsonIgnore
    private Long id;
    
    private String name;
    
    private Integer age;
}
