package com.ruoyi.system.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * 业务层
 * 
 * @author ruoyi
 */
@FeignClient(name = "elasticSearch")
public interface IElasticSearchService {
    
    // 测试查询所有
    @GetMapping("/elasticSearch/matchQueryAll")
    public List<Object> matchQueryAll();

}
