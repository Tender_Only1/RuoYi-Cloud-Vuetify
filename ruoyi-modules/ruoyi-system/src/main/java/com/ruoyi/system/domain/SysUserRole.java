package com.ruoyi.system.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户和角色关联 sys_user_role
 * 
 * @author ruoyi
 */
@ApiModel(value = "用户和角色关联")
@Data
public class SysUserRole {

    @ApiModelProperty(value = "用户ID")
    private Long userId;
    
    @ApiModelProperty(value = "角色ID")
    private Long roleId;

}
