package com.ruoyi.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 路由显示信息
 * 
 * @author ruoyi
 */
@ApiModel(value = "路由显示信息")
@Data
public class MetaVo {

    @ApiModelProperty(value = "设置该路由在侧边栏和面包屑中展示的名字")
    private String title;

    @ApiModelProperty(value = "设置该路由的图标, 对应路径src/assets/icons/svg")
    private String icon;

    @ApiModelProperty(value = "设置为true, 则不会被 <keep-alive>缓存")
    private boolean noCache;

    public MetaVo() {
    }

    public MetaVo(String title, String icon) {
        this.title = title;
        this.icon = icon;
    }

    public MetaVo(String title, String icon, boolean noCache) {
        this.title = title;
        this.icon = icon;
        this.noCache = noCache;
    }

}
