package com.ruoyi.system.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.annotation.Excel.ColumnType;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 岗位/职位/角色表 sys_post
 * 
 * @author ruoyi
 */
@ApiModel(value = "岗位/职位表/角色")
@Data
@EqualsAndHashCode(callSuper = true)
public class SysPost extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "序号")
    @Excel(name = "序号", cellType = ColumnType.NUMERIC)
    private Long postId;

    /** 岗位编码 */
    @ApiModelProperty(value = "编码")
    private String postCode;

    @ApiModelProperty(value = "名称")
    @Excel(name = "名称")
    private String postName;

    @ApiModelProperty(value = "排序")
    @Excel(name = "岗位排序")
    private String postSort;

    @ApiModelProperty(value = "状态(1正常 0停用)")
    @Excel(name = "状态", readConverterExp = "1=正常,0=停用")
    private String status;

    @ApiModelProperty(value = "用户是否存在此岗位标识 默认不存在")
    private boolean flag = false;

    @NotBlank(message = "岗位编码不能为空")
    @Size(min = 0, max = 64, message = "岗位编码长度不能超过64个字符")
    public String getPostCode() {
        return postCode;
    }

    @NotBlank(message = "岗位名称不能为空")
    @Size(min = 0, max = 50, message = "岗位名称长度不能超过50个字符")
    public String getPostName() {
        return postName;
    }

    @NotBlank(message = "显示顺序不能为空")
    public String getPostSort() {
        return postSort;
    }

}
