package com.ruoyi.common.core.enums;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户状态
 * 
 * @author ruoyi
 */
@ApiModel(value = "用户状态")
public enum UserStatus {

    @ApiModelProperty(value = "状态")
    OK("1", "正常"), DISABLE("0", "停用"), DELETED("2", "删除");

    @ApiModelProperty(value = "状态码")
    private final String code;
    @ApiModelProperty(value = "信息")
    private final String info;

    UserStatus(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }
}
