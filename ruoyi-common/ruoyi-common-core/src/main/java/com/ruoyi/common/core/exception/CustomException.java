package com.ruoyi.common.core.exception;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 自定义异常
 * 
 * @author ruoyi
 */
@ApiModel(value = "自定义异常")
public class CustomException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "错误码")
    private Integer code;

    @ApiModelProperty(value = "错误消息")
    private String message;

    public CustomException(String message) {
        this.message = message;
    }

    public CustomException(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

    public CustomException(String message, Throwable e) {
        super(message, e);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }
}
