package com.ruoyi.common.core.exception;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 基础异常
 * 
 * @author ruoyi
 */
@ApiModel(value = "基础异常")
public class BaseException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "所属模块")
    private String module;

    @ApiModelProperty(value = "错误码")
    private String code;

    @ApiModelProperty(value = "错误码对应的参数")
    private Object[] args;

    @ApiModelProperty(value = "错误消息")
    private String defaultMessage;

    public BaseException(String module, String code, Object[] args, String defaultMessage) {
        this.module = module;
        this.code = code;
        this.args = args;
        this.defaultMessage = defaultMessage;
    }

    public BaseException(String module, String code, Object[] args) {
        this(module, code, args, null);
    }

    public BaseException(String module, String defaultMessage) {
        this(module, null, null, defaultMessage);
    }

    public BaseException(String code, Object[] args) {
        this(null, code, args, null);
    }

    public BaseException(String defaultMessage) {
        this(null, null, null, defaultMessage);
    }

    public String getModule() {
        return module;
    }

    public String getCode() {
        return code;
    }

    public Object[] getArgs() {
        return args;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }
}
