package com.ruoyi.auth.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户登录对象
 * 
 * @author ruoyi
 */
@ApiModel(value = "用户登录对象")
@Data
public class LoginBody {

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "用户密码")
    private String password;

}
