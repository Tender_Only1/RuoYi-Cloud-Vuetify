package com.ruoyi.system.api.model;

import java.io.Serializable;
import java.util.Set;
import com.ruoyi.system.api.domain.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户信息
 *
 * @author ruoyi
 */
@ApiModel(value = "用户信息")
@Data
public class LoginUser implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户唯一标识")
    private String token;

    @ApiModelProperty(value = "用户名id")
    private Long userid;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "登录时间")
    private Long loginTime;

    @ApiModelProperty(value = "过期时间")
    private Long expireTime;

    
    @ApiModelProperty(value = "登录IP地址")
    private String ipaddr;

    @ApiModelProperty(value = "权限列表")
    private Set<String> permissions;

    @ApiModelProperty(value = "角色列表")
    private Set<String> roles;

    @ApiModelProperty(value = "用户信息")
    private SysUser sysUser;
    
}
