package com.ruoyi.system.api.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 文件信息
 * 
 * @author ruoyi
 */
@ApiModel(value = "文件信息")
@Data
public class SysFile {

    @ApiModelProperty(value = "文件名称")
    private String name;

    @ApiModelProperty(value = "文件地址")
    private String url;

}
