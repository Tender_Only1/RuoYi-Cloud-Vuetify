package com.ruoyi.system.api.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.annotation.Excel.ColumnType;
import com.ruoyi.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 操作日志记录表 oper_log
 * 
 * @author ruoyi
 */
@ApiModel(value = "操作日志记录")
@Data
@EqualsAndHashCode(callSuper = true)
public class SysOperLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "日志主键")
    @Excel(name = "操作序号", cellType = ColumnType.NUMERIC)
    private Long operId;

    @ApiModelProperty(value = "操作模块")
    @Excel(name = "操作模块")
    private String title;

    @ApiModelProperty(value = "业务类型(0其它 1新增 2修改 3删除)")
    @Excel(name = "业务类型", readConverterExp = "0=其它,1=新增,2=修改,3=删除,4=授权,5=导出,6=导入,7=强退,8=生成代码,9=清空数据")
    private Integer businessType;

    @ApiModelProperty(value = "业务类型数组")
    private Integer[] businessTypes;

    @ApiModelProperty(value = "请求方法")
    @Excel(name = "请求方法")
    private String method;

    @ApiModelProperty(value = "请求方式")
    @Excel(name = "请求方式")
    private String requestMethod;

    @ApiModelProperty(value = "操作类别(0其它 1后台用户 2手机端用户)")
    @Excel(name = "操作类别", readConverterExp = "0=其它,1=后台用户,2=手机端用户")
    private Integer operatorType;

    @ApiModelProperty(value = "操作人员")
    @Excel(name = "操作人员")
    private String operName;

    @Excel(name = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "请求url")
    @Excel(name = "请求地址")
    private String operUrl;

    @ApiModelProperty(value = "操作地址")
    @Excel(name = "操作地址")
    private String operIp;

    @ApiModelProperty(value = "请求参数")
    @Excel(name = "请求参数")
    private String operParam;

    @ApiModelProperty(value = "返回参数")
    @Excel(name = "返回参数")
    private String jsonResult;

    @ApiModelProperty(value = "操作状态(1正常 0异常)")
    @Excel(name = "状态", readConverterExp = "1=正常,0=异常")
    private Integer status;

    @ApiModelProperty(value = "错误消息")
    @Excel(name = "错误消息")
    private String errorMsg;

    @ApiModelProperty(value = "操作时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date operTime;

}
